import React, { Component } from "react";
import "../App.css";
import { connect } from "react-redux";

import { searchMovie, fetchMovies, setLoading } from "../actions/searchActions";

export class SearchForm extends Component {
  componentDidMount() {
    this.props.fetchMovies("money");
  }
  onChange = e => {
    this.props.searchMovie(e.target.value);
  };

  onSubmit = e => {
    e.preventDefault();
    if (this.props.text === "") {
      alert("Fill the form");
    }
    this.props.fetchMovies(this.props.text);
    this.props.setLoading();
  };

  render() {
    return (
      <div className="wrapper">
        <div className="container-search">
          <h1>MovieScore</h1>
          <form
            className="container-input"
            id="searchForm"
            onSubmit={this.onSubmit}
          >
            <input
              onChange={this.onChange}
              name="searchText"
              type="text"
              id="searchInput"
              className="search-term"
              placeholder="Start typing.."
              style={{ height: 54 }}
            />
            <button type="submit" className="search-button" id="selectTheory">
              <i className="fa fa-search"></i>
            </button>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  text: state.movies.text
});

export default connect(mapStateToProps, {
  searchMovie,
  fetchMovies,
  setLoading
})(SearchForm);
