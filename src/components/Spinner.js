import React from "react";

function Spinner() {
  return (
    <div className="svgImage">
      <svg width="200" height="200" viewBox="0 0 20 50">
        <polygon
          className="triangle"
          fill="none"
          stroke="#fff"
          strokeWidth="1"
          points="16,1 32,32 1,32"
        />
      </svg>
    </div>
  );
}

export default Spinner;
