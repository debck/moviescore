import React, { Component } from "react";
import { Link } from "react-router-dom";
export class MovieCard extends Component {
  render() {
    const { movie } = this.props;
    return (
      <div className="col-md-4">
        <div className="card">
          <div className="card-header">
            <img className="card-img" src={movie.Poster} alt="Card" />
          </div>
          <div className="card-body">
            <h3 className="card-title">{movie.Title}</h3>
            <Link className="trailer-preview" to={"/movie/" + movie.imdbID}>
              <i className="fa fa-ellipsis-h" aria-hidden="true"></i>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

export default MovieCard;
