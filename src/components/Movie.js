import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import { fetchMovie, setLoading } from "../actions/searchActions";

import Spinner from "./Spinner";

export class Movie extends Component {
  componentDidMount() {
    this.props.fetchMovie(this.props.match.params.id);
    this.props.setLoading();
  }
  render() {
    const { loading, movie } = this.props;
    console.log(movie);
    let movieInfo = (
      <div className="container">
        <div className="card-detailsme">
          <div className="container-fliud">
            <div className="wrapper row">
              <div className="preview col-md-6">
                <div className="preview-pic tab-content">
                  <div className="tab-pane active detailsmovie" id="pic-1">
                    <img src={movie.Poster} alt="Poster" />
                  </div>
                </div>
              </div>
              <div className="details col-md-6">
                <h3 className="product-title">{movie.Title}</h3>
                <div className="rating">
                  <span className="review-no">
                    {movie.imdbRating} Imdb Rating | {movie.imdbVotes} Imdb
                    Votes
                  </span>
                </div>
                <p className="vote">
                  <strong>{movie.Genre}</strong>
                </p>
                <h6 className="sizes">Plot:</h6>
                <p className="product-description">{movie.Plot}</p>

                <h6 className="sizes">
                  Director:
                  <span className="size" data-toggle="tooltip" title="small">
                    {movie.Director}
                  </span>
                </h6>
                <h6 className="sizes">
                  Writer:
                  <span className="size" data-toggle="tooltip" title="small">
                    {movie.Writer}
                  </span>
                </h6>
                <h6 className="sizes">
                  Actors:
                  <span className="size" data-toggle="tooltip" title="small">
                    {movie.Actors}
                  </span>
                </h6>
                <h6 className="sizes">
                  Language:
                  <span className="size" data-toggle="tooltip" title="small">
                    {movie.Language}
                  </span>
                </h6>
                <h6 className="sizes">
                  Country:
                  <span className="size" data-toggle="tooltip" title="small">
                    {movie.Country}
                  </span>
                </h6>
                <h6 className="sizes">
                  Awards:
                  <span className="size" data-toggle="tooltip" title="small">
                    {movie.Awards}
                  </span>
                </h6>
                <h6 className="sizes">
                  Released:
                  <span className="size" data-toggle="tooltip" title="small">
                    {movie.Released}
                  </span>
                </h6>
                <h6 className="sizes">
                  Production:
                  <span className="size" data-toggle="tooltip" title="small">
                    {movie.Production}
                  </span>
                </h6>
                <Link className="" to={"/"}>
                  <i className="fa fa-arrow-left fa-2x" aria-hidden="true"></i>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    );

    let content = loading ? <Spinner /> : movieInfo;
    return <div>{content}</div>;
  }
}

const mapStateToProps = state => ({
  loading: state.movies.loading,
  movie: state.movies.movie
});

export default connect(mapStateToProps, { fetchMovie, setLoading })(Movie);
